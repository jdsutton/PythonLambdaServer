import('/src/JSUtils/util/DataObject.js');

/**
 * @class
 */
class [[CLASS]] extends DataObject {
    
    /**
     * @constructor
     */
    constructor(obj) {
        super(obj, false, true);
    }
    
    /**
     * @public
     */
    init() {

    }

    /**
     * @public
     */
    static fromFormData(data) {
        let result = new [[CLASS]](data);


        return result;
    }

    /**
     * @public
     */
    toFormData() {
        let result = Object.assign({}, this);


        return result;
    }
}