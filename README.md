# Python Lambda Server
Host your Python 3.6 AWS lambdas locally for development and testing purposes.

## 1. (Optional) Create a service
Manually copy this example: [Example](https://gitlab.com/jdsutton/PythonLambdaServer/blob/master/Example_Service.md)

OR

`PythonLambdaServer/scripts/expandAPI.py`

## 2. Create a lambda
You may skip this step if you ran the script in step 1.

`path/to/my/lambda/code/myLambda/lambda_function.py`

```
from mypackage.MyService import MyService

def lambda_handler(event, context):
    # If you did step 1.
    return MyService.handleEvent(event)
    
    # Else, do something else with the event object.
```

## 3. Start the lambda server

### From the command line
`$ ./pythonlambdaserver/LambdaServer.py --directory <path/to/my/lambda/code> --port <desired port>`

### From Python

```
from pythonlambdaserver.LambdaServer import LambdaServerApp

directory = '/path/to/my/services'
port = 5001

LambdaServerApp.run(directory, port)
```

## 4. Call the backend from a frontend client
Instantiate a [BackendService](https://gitlab.com/jdsutton/PythonLambdaServer/blob/master/BackendService.js) and call .invoke() on it.