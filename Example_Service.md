# Example Service
A service class for enumerating methods which may be called by clients.

```
from pythonlambdaserver.LambdaService import LambdaService
from pythonlambdaserver.LambdaServiceMethodConfig import LambdaServiceMethodConfig

class MyService(LambdaService):
    '''
    TODO.
    '''

    METHOD_CONFIGURATION = {
        # Visible to clients as "instanceOfClassX.methodName"
        'instanceOfClassX': {
            'methodName': LambdaServiceMethodConfig(True),
        },
    }

    @property
    def instanceOfClassX(self):
        class X:
            def methodName(self):
                return 1
                
        return X()
```