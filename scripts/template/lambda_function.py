from eekquery.ConfigFactory import ConfigFactory
from eekquery.RDBClientFactory import RDBClientFactory
from pythonlambdaserver.LambdaService import LambdaService
from pythonlambdaserver.LambdaServiceMethodConfig import LambdaServiceMethodConfig

class [[SERVICENAME]](LambdaService):
    '''
    TODO.
    '''

    METHOD_CONFIGURATION = {
        'default': {
            [[METHODS]]
        }
    }

    @property
    def default(self):
        from [[SMART_TABLE]] import [[CLASS_PLURAL]]

        config = ConfigFactory.produce('')
        client = RDBClientFactory.produce(config)

        return [[CLASS_PLURAL]](client)

def lambda_handler(event, context):
    return [[SERVICENAME]].handleEvent(event, context)