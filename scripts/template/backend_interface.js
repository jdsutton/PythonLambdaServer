import('[[CLASS]].js');
import('/src/JSUtils/backend/BackendService.js');

const [[CLASS_PLURAL]] = BackendService.declare(
    '[[CLASS_PLURAL]]Service',
    [[CLASS]],
    ).forAllRequests(() => {
        return {};
    });