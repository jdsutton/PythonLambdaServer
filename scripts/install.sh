#!/bin/bash

cp etc/init.d/lambdaserver /etc/init.d
cp sites-enabled/lambda /etc/nginx/sites-enabled

update-rc.d lambdaserver defaults
systemctl daemon-reload

virtualenv -ppython3.6 env

. env/bin/activate

pip install --upgrade .

service lambdaserver start
