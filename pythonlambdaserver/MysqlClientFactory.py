from eekquery.MysqlClient import MysqlClient
from eekquery.MysqlConfig import MysqlConfig
import os

class MysqlClientFactory:
    '''
    Produces MysqlClients.
    '''

    def __init__(self, config):
        self._clientsByPID = dict()
        self._config = config

    def produce(self):
        '''
        Returns a pre-configured MysqlClient.
        '''

        pid = os.getpid()

        if pid in self._clientsByPID:
            return self._clientsByPID[pid]

        dbConfig = MysqlConfig(
            self._config.dbUsername,
            self._config.dbPassword,
            self._config.dbName,
            self._config.dbEndpoint,
            charset=self._config.dbCharset,
        )

        client = MysqlClient(config=dbConfig)

        self._clientsByPID[pid] = client

        return client
