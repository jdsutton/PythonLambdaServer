#!/usr/bin/python3

from argparse import ArgumentParser
import cgi
import glob
from http.server import BaseHTTPRequestHandler, HTTPServer
import os
from os.path import join, sep, expanduser
import json
import logging
from pythonlambdaserver import LambdaExecutor
import signal
from socketserver import ForkingTCPServer, TCPServer
import subprocess
import sys
import traceback
import time
import uuid
from urllib.parse import parse_qs, urlparse

class ReusePortServer(ForkingTCPServer):
    allow_reuse_address = True

class TCPReuseServer(TCPServer):
    allow_reuse_address = True

class LambdaServer:
    '''
    A multithreaded server for running Python lambdas.
    '''

    DEFAULT_FILE_UPLOAD_DIR = expanduser('~/lambda_file_upload/')

    def __init__(self, directory, fileUploadDirectory=DEFAULT_FILE_UPLOAD_DIR):
        directory = [expanduser(d) for d in directory]
        
        self._dir = directory
        self._lambdas = LambdaServer.listLambdas(self._dir)
        self._server = None
        LambdaServer._fileUploadDirectory = expanduser(fileUploadDirectory)

        os.makedirs(LambdaServer._fileUploadDirectory, exist_ok=True)

        print('Found lambdas: {}'.format(self._lambdas))
        print('Storing uploaded files in {}'.format(LambdaServer._fileUploadDirectory))

    @staticmethod
    def listLambdas(directories):
        '''
        Recursively find all lambdas under a directory.
        Looks for files like: **/my_lambda_name/lambda_function.py
        '''

        result = dict()

        def ignore(d):
            if d == 'node_modules':
                return True

            if d == '__pycache__':
                return True

            return False

        for d in directories:
            for directory, subdirectories, files in os.walk(d, followlinks=True):
                subdirectories[:] = [s for s in subdirectories if not ignore(d)]

                if 'lambda_function.py' in files:
                    lambdaName = directory.split(sep)[-1]
                    result[lambdaName] = join(directory, 'lambda_function.py')

        return result

    def callLambda(self, lambdaName, eventData, context):
        '''
        Calls a lambda function by name
        '''

        lambdaF = self._lambdas[lambdaName]

        return LambdaExecutor.executeLambda(lambdaF, eventData, context)

    class RequestHandler(BaseHTTPRequestHandler):

        allowedOrigins = None

        def sendJson(self, s):
            if not isinstance(s, str):
                try:
                    s = json.dumps(s)
                except Exception as e:
                    logging.error(e)

                    s = str(e)

            self.send_response(200)
            self._handleCorsHeaders()
            self.send_header('Content-Type', 'application/json')
            self.end_headers()

            self.wfile.write(s.encode())

        def _writeFile(self):
            form = cgi.FieldStorage(
                fp=self.rfile,
                headers=self.headers,
                environ={
                    'REQUEST_METHOD': 'POST',
                    'CONTENT_TYPE': self.headers['Content-Type'],
                },
            )

            uploadedName = form['file'].filename
            ext = uploadedName.split('.')[-1]
            filename = str(uuid.uuid4().hex + '.' + ext)
            data = form['file'].file.read()
            out = join(LambdaServer._fileUploadDirectory, filename)

            with open(out, 'wb') as f:
                f.write(data)

            self.sendJson({
                'fileName': filename,
            })

        def _handleCorsHeaders(self):
            origin = self.headers.get('Origin', None) or self.headers.get('origin', None)
            origins = self.__class__.allowedOrigins or []

            allowed = origin in origins
            allowAll = '*' in origins

            if allowed or allowAll:
                self.send_header('Access-Control-Allow-Origin', origin)

        def do_HEAD(self):
            self.send_response(200)
            self._handleCorsHeaders()
            self.end_headers()

        def do_GET(self):
            self.send_response(200)
            self._handleCorsHeaders()
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write('OK'.encode())

        def do_OPTIONS(self):
            self.send_response(200)
            self._handleCorsHeaders()
            self.end_headers()
            
        def do_POST(self):
            ctype, pdict = cgi.parse_header(self.headers['content-type'])
            
            if ctype == 'multipart/form-data':
                self._writeFile()

                return
            elif ctype == 'application/x-www-form-urlencoded':
                length = int(self.headers['content-length'])
                postvars = parse_qs(self.rfile.read(length), keep_blank_values=1)
            else:
                postvars = {}
            
            try:
                try:
                    lambdaName = postvars['FunctionName'.encode()][0].decode()
                    payload = postvars['Payload'.encode()][0].decode()
                except:
                    result = {
                        'errorMessage': 'Bad arguments. Make sure to provide FunctionName and Payload. Arguments were: {}'.format(postvars)
                    }
                    self.sendJson(result)

                    return

                context = dict(self.headers)
                context['lambdaName'] = lambdaName
                
                result = LambdaServerApp.server.callLambda(lambdaName, payload, context)
            except Exception as e:
                logging.error(traceback.format_exc())
                logging.error(str(postvars))

                result = e

            self.sendJson(result)

    def run(self, port, *, allowedOrigins=None, stateful=False):
        server_address = ('', port)

        if allowedOrigins is not None:
            allowedOrigins = set(allowedOrigins)

        LambdaServer.RequestHandler.allowedOrigins = allowedOrigins

        if stateful:
            self._server = TCPReuseServer(server_address, LambdaServer.RequestHandler)
        else:
            self._server = ReusePortServer(server_address, LambdaServer.RequestHandler)

        self._server.serve_forever()

    def close(self):
        if self._server is not None:
            self._server.server_close()

class LambdaServerApp:
    server = None

    @staticmethod
    def parseArgs():
        parser = ArgumentParser()
        parser.add_argument('--directory', type=str, nargs='+',
            help='Parent directory containing your Python lambda code.')
        parser.add_argument('--port', type=int,
            help='Port to listen to for incoming lambda calls.')

        return parser.parse_args()

    @staticmethod
    def run(directory, port, *, allowedOrigins=None, stateful=False):
        LambdaServerApp.server = LambdaServer(directory)

        signal.signal(signal.SIGINT, LambdaServerApp.close)

        LambdaServerApp.server.run(port, allowedOrigins=allowedOrigins, stateful=stateful)

    @staticmethod
    def close():
        print('Shutting down server.')
        LambdaServerApp.server.close()
        sys.exit(0)

if __name__ == '__main__':
    args = LambdaServerApp.parseArgs()
    LambdaServerApp.run(args.directory, args.port)
