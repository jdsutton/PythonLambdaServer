import('/src/JSUtils/util/Request.js');

/**
 * @class
 */
class BackendService {

    /**
     * @constructor
     */
    constructor(lambdaFunctionName, methodPrefix) {
        this._lambdaFunctioName = lambdaFunctionName;
        this._methodPrefix = methodPrefix;
    }

    /**
     * @public
     */
    invoke(method, args, kwargs) {
        const params = {
            FunctionName: this._lambdaFunctioName,
            Payload: JSON.stringify({
                method: this._methodPrefix + '.' + method,
                args,
                kwargs,
            }),
        };

        return Request.post(this.constructor._ENDPOINT, params, {}, true).then(response => {
            const parsed = JSON.parse(response);

            if (parsed && parsed.errorMessage) {
                throw new Error(parsed.errorMessage);
            }

            return parsed;
        });
    }
}

BackendService._ENDPOINT = '[["backendEndpoint"]]';
