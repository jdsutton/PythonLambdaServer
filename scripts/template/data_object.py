from eekquery.DataObject import DataObject

class [[CLASS]](DataObject):

    TABLE = '[[CLASS_PLURAL]]'

    def __init__(self, fromDict):

        super().__init__(fromDict)

        self.addField('id', isKey=True)

