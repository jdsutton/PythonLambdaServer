from contextlib import contextmanager
from importlib import util
import json
import logging
import os
import sys
import traceback

class LambdaExecutor:

    class IgnoreRequestException(RuntimeError):
        pass

# https://stackoverflow.com/questions/41861427/python-3-5-how-to-dynamically-import-a-module-given-the-full-file-path-in-the
@contextmanager
def add_to_path(p):
    import sys
    old_path = sys.path
    sys.path = sys.path[:]
    sys.path.insert(0, p)
    try:
            yield
    finally:
            sys.path = old_path

def path_import(absolute_path):
    '''
    implementation taken from https://docs.python.org/3/library/importlib.html#importing-a-source-file-directly
    '''

    with add_to_path(os.path.dirname(absolute_path)):
        spec = util.spec_from_file_location(absolute_path, absolute_path)
        module = util.module_from_spec(spec)
        spec.loader.exec_module(module)

        return module

def executeLambda(lambdaFilePath, eventString, context):
    try:
        event = json.loads(eventString)
        lambdaFile = path_import(lambdaFilePath)
        
        result = lambdaFile.lambda_handler(event, context)
    except LambdaExecutor.IgnoreRequestException:
        # Don't send a response.
        # Useful if forked.
        return
    except BaseException as e:
        result = dict()
        lambdaName = lambdaFilePath.split(os.path.sep)[-2]
        logFile = os.path.join(os.getcwd(), 'logs', lambdaName + '.txt')
        os.makedirs(os.path.dirname(logFile), exist_ok=True)

        logger = logging.getLogger(__name__)
        handler = logging.FileHandler(logFile)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s\n%(message)s')
        handler.setFormatter(formatter)

        logger.addHandler(handler)
        logger.setLevel(logging.DEBUG)
        logger.error(traceback.format_exc())

        result['errorMessage'] = str(e)

    return json.dumps(result)

def main(lambdaFilePath, eventString):
    print(executeLambda(lambdaFilePath, eventString))
    
if __name__ == '__main__':
        main(*sys.argv[1:])