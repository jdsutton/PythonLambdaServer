from decimal import Decimal
from enum import Enum
import json
import logging
from pythonlambdaserver.LambdaServiceMethodConfig import LambdaServiceMethodConfig
from py3typing.Typing import Params, Returns

class LambdaService:
    '''
    Represents a service
    '''

    METHOD_CONFIGURATION = dict()
    context = None

    @classmethod
    def integrate(cls, otherLambdaService):
        '''
        TODO.
        '''

        for key, value in otherLambdaService.METHOD_CONFIGURATION.items():
            cls.METHOD_CONFIGURATION[key] = value
    
    @staticmethod
    @Params(str, dict)
    @Returns(LambdaServiceMethodConfig)
    def getMethodConfig(methodString, methodConfig):
        '''
        Return the `LambdaServiceMethodConfig` for this `methodString`.
        '''
        
        methodTokens = methodString.split('.')
        
        for token in methodTokens:
            if token not in methodConfig:
                
                return LambdaServiceMethodConfig(False)
            
            methodConfig = methodConfig[token]
        
        return methodConfig
    
    @classmethod
    def getMethod(cls, methodString):
        method = methodString.split('.')
        attr = method.pop(0)
        '''
        Safe because only specific strings are allowed.
        Change with caution. Arbitrary strings must not be passed to eval().
        '''
        parent = eval('cls().' + attr)
        
        for attr in method:
            parent = getattr(parent, attr)
         
        return parent

    @staticmethod
    def _toDict(obj, classkey=None):
        #pylint: disable=line-too-long
        '''
        Converts an object to a dict.
        Adapted from: https://stackoverflow.com/questions/1036409/recursively-convert-python-object-graph-to-dictionary
        '''

        if hasattr(obj, 'items') and hasattr(obj.items, '__call__'):
            data = {}
            
            for (k, v) in obj.items():
                data[k] = LambdaService._toDict(v, classkey)

            return data
        elif isinstance(obj, Enum):
            return obj.value
        elif hasattr(obj, '_ast'):
            return LambdaService._toDict(obj._ast())
        elif hasattr(obj, '__iter__') and not isinstance(obj, str):
            return [LambdaService._toDict(v, classkey) for v in obj]
        elif hasattr(obj, '__dict__'):
            data = dict([
                (key, LambdaService._toDict(value, classkey))
                for key, value in obj.__dict__.items()
                if not callable(value) and not key.startswith('_')
            ])

            if classkey is not None and hasattr(obj, '__class__'):
                data[classkey] = obj.__class__.__name__

            return data
        elif isinstance(obj, Decimal):
            return float(obj)
        else:
            return obj

    @classmethod
    def handleEvent(cls, event, context=None):
        '''
        Looks up and calls the method given.

        event: A dict payload from a lambda call.
        event.method: Qualified method name to evaluate, eg: 'facilitiesTable.getFacility'.
            See the METHOD_CONFIGURATION on the relevant service implementation.
        event.args: A list of positional arguments to be passed to the method.
        event.kwargs: A dict of keyword arguments to be passed to the method.
        '''

        logging.basicConfig(level=logging.INFO)
        logging.info(json.dumps(event))

        if 'ping' in event:
            return 'OK'

        LambdaService.context = context
        
        # Required.
        method = event['method']

        # Optional.
        args = event.get('args', [])
        kwargs = event.get('kwargs', {})

        methodConfig = cls.getMethodConfig(method, cls.METHOD_CONFIGURATION)
        
        if not methodConfig.isAuthorized:
            message = 'Error: Not authorized to invoke `{}`.'.format(method)
            raise ValueError(message)
        
        method = cls.getMethod(method)

        result = method(*args, **kwargs)
        result = LambdaService._toDict(result)

        return result
