
import json
import requests

class LambdaRequestSender:

    @staticmethod
    def sendRequest(endpoint, functionName, method, args, kwargs):
        '''
        Sends a lambda request via HTTP.
        '''

        payload = {
            'FunctionName': functionName,
            'Payload': json.dumps({
                'method': method,
                'args': args,
                'kwargs': kwargs,
            }),
        }

        result = requests.post(
            endpoint,
            data=payload,
            headers={'content-type': 'application/x-www-form-urlencoded'},
        )

        return result.json()