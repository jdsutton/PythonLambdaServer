from eekquery.DataObject import DataObject
import json
import logging

class ConfigFactory:
    '''
    Returns configuration details.
    '''

    class ENV:
        TEST = 'test'
        DEV = 'dev'

    def __init__(self, configPath):

        self._config = DataObject({
            'dbUsername': 'root',
            'dbPassword': 'root',
            'dbName': 'TestDB',
            'dbEndpoint': '127.0.0.1',
            'dbCharset': 'utf8mb4',
            'environment': 'test',
        }, autoField=True)

        self._originalDBName = self._config.dbName

        try:
            with open(configPath) as f:
                lines = f.read()

            self._config = DataObject(json.loads(lines),
                autoField=True)
        except:
            logging.warning('Failed to load config file: {}'.format(configPath))
            self._config.dbName = self._config.dbName + '_test'

    def produce(self):
        return self._config

    def setEnvironment(self, env):
        self._config.dbName = self._originalDBName + '_' + env
