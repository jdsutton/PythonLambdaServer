from datetime import timedelta
from py3typing.Typing import optional, Params, Returns

class LambdaServiceMethodConfig:
    '''
    Service method configuration.
    '''
    
    @Params(object, optional(bool), optional(timedelta))
    def __init__(self, isAuthorized=True, maxAge=timedelta(days=1)):
        '''
        Constructor.
        '''
        
        self.isAuthorized = isAuthorized
        self.maxAge = maxAge
