#!/usr/bin/python3

'''
Script to create new services and methods.
'''

import os

class ExpandAPI:

    @staticmethod
    def confirmResponse(prompt, default=None):
        while True:
            preview = ''

            if default is not None:
                preview = ' ({})'.format(default)

            response = input('{}{} > '.format(prompt, preview))

            if len(response) == 0 and default is not None:
                response = default

            confirm = input('Confirm spelling: <{}>, correct? (Y/n)'.format(response))

            if confirm in ['', 'Y', 'y', 'yes']:
                

                return response

    @staticmethod
    def main():
        cwd = os.getcwd()
        projectPath = os.path.expanduser(ExpandAPI.confirmResponse('Enter project path', cwd))

        serviceName = ExpandAPI.confirmResponse('Enter service name')

        if not serviceName.endswith('Service'):
            serviceName = serviceName + 'Service'

        module = ExpandAPI.confirmResponse('Enter data class import path')

        className = module.split('.')[-1]
        package = module.split('.')[0]

        pluralClass = className

        if pluralClass.endswith('y'):
            pluralClass = pluralClass[:-1] + 'ies'

        if not pluralClass.endswith('s'):
            pluralClass += 's'

        # Methods.
        methodNames = ExpandAPI.confirmResponse('Enter method name(s) (comma separated)').split(',')
        methods = ''

        for method in methodNames: 
            methods += '\'{}\': LambdaServiceMethodConfig(True),\n'.format(method)

        methods = methods[0:len(methods) - 1]
        smartTable = '.'.join([package, pluralClass.lower(), pluralClass])

        templateValues = {
            '[[MODULE]]': module,
            '[[SMART_TABLE]]': smartTable,
            '[[CLASS]]': className,
            '[[CLASS_PLURAL]]': pluralClass,
            '[[SERVICENAME]]': serviceName,
            '[[METHODS]]': methods,
        }

        def writeTemplateTo(templatePath, values, outPath):
            with open(templatePath, 'r') as f:
                template = f.read()

            for key, value in values.items():
                template = template.replace(key, str(value))

            os.makedirs(os.path.dirname(outPath), exist_ok=True)

            # TODO: Don't overwrite if exists.
            with open(outPath, 'w') as f:
                f.write(template)

        ownPath = os.path.dirname(os.path.realpath(__file__))

        templatePath = os.path.join(ownPath, 'template', 'lambda_function.py')
        outPath = os.path.join(projectPath, 'services', serviceName, 'lambda_function.py')

        writeTemplateTo(templatePath, templateValues, outPath)

        # DataObject.
        templatePath = os.path.join(ownPath, 'template', 'data_object.py')
        outPath = os.path.join(projectPath, package, pluralClass.lower(), className + '.py')

        writeTemplateTo(templatePath, templateValues, outPath)

        # SmartTable
        templatePath = os.path.join(ownPath, 'template', 'smart_table.py')
        outPath = os.path.join(projectPath, package, pluralClass.lower(), pluralClass + '.py')

        writeTemplateTo(templatePath, templateValues, outPath)

        # Create JS interface.
        templatePath = os.path.join(ownPath, 'template', 'backend_interface.js')
        
        outPath = os.path.join(projectPath, 'src', pluralClass.lower(), '{}.js'.format(pluralClass))

        writeTemplateTo(templatePath, templateValues, outPath)

        # Create JS DataObject.
        templatePath = os.path.join(ownPath, 'template', 'data_object.js')
        outPath = os.path.join(projectPath, 'src', pluralClass.lower(), '{}.js'.format(className))

        writeTemplateTo(templatePath, templateValues, outPath)

if __name__ == '__main__':
    ExpandAPI.main()